#ifndef CYPHER_H
#define CYPHER_H

#include <string>
using namespace std;

string cypher(string, string) ;
string decypher(string message, string key);


#endif // CYPHER_H

